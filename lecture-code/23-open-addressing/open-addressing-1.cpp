#include <iostream>
#include <vector>

using namespace std;

#define EMPTY_SENTINEL -1

template <typename T>
class OATable{
private:
    size_t tsize;
    vector<T> table;

    // assume: table is not full.
    // assume: T is something that works with the mod operation(like not string)
    // assume: the table already has EMPTY_SENTINEL in all empty buckets
    // locate behavior : returns where value exists, or where value would go
    int locate(const T value) { // uses hash and tsize and EMPTY_SENTINEL
        int bucket = hash(value) % tsize;
        while(table[bucket] != EMPTY_SENTINEL && table[bucket] != value){
            bucket = (bucket +1 ) % tsize;
        }
        return bucket;
    }

    void resize(){
        //TODO
    }

public:

    T hash(const T value){
        return value;
    }

    void insert(const T value){
        int bucket = locate(value);
        table[bucket] = value;
    }
    bool find(const T value){
        int bucket = locate(value);
        if (table[bucket] == value)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

};
int main()
{
    return 0;
}
