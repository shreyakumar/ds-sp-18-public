// open addressing 2
#include <iostream>

using namespace std;

//TODO class OATable

#define EMPTY_SENTINEL -1

template <typename T>
class OATable{
    private:
        size_t tsize;
        vector<T> table;

        // assume: all empty buckets have EMPTY_SENTINEL
        // assume: the table is not full
        // assume: that T is int
        // assume: that value is not duplicated
        // locate returns the bucket where value is, or where value should go
        int locate(const T value){ // uses tsize, hash and EMPTY_SENTINEL
            int bucket = hash(value) % tsize;
            while(table[bucket] != EMPTY_SENTINEL && table[bucket] != value){
                bucket = (bucket+1) % tsize;
            }
            return bucket;
        }

        void resize(){
            //TODO ICE part 2
        }
    public:
        void insert(const T value){
            int bucket = locate(value);
            table[bucket] = value;
        }
        bool find(const T value){
            int bucket = locate(value);
            if (table[bucket] == value){return true;}
            else{return false;}

        }
        T hash(const T value){
            return value;
        }
}

int main(int argc, char const *argv[]) {
    OATable<T> s;
    //TODO

    return 0;
}
