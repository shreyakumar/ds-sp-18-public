#include <iostream>
#include <unordered_set> // unordered set
using namespace std;

int main(int argc, char const *argv[]) {
    // declare set
    unordered_set<int> s;

    // get number of items cmd line
    if (argc != 2){
        cerr << "usage: " << argv[0] << " nitems" << endl;
        return 1;
    }
    int nitems = atoi(argv[1]);
    //insert that many items into the set
    for (size_t i = 0; i < nitems; i++) {
        s.insert(i);
    }

    // find items in the set
    for (size_t i = 0; i < nitems; i++) {
        s.find(i);
    }

    return 0;
}
