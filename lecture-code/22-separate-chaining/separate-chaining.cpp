#include <iostream>
#include <vector>
#include <set>

using namespace std;

int TABLESIZE = 1<<5;

template <typename T>
class SCTable {
public:
    SCTable(int size= TABLESIZE){
        tsize = size;
        table = vector<set<T> >(tsize);
    }

    T hash(const T &value){
        return value;
    }

    void insert(const T &value){ // hash table's insert function
        int bucket = hash(value) % tsize;
        table[bucket].insert(value); // this insert is set's insert function
    }

    bool search(const T &value){
        int bucket = hash(value) % tsize;
        return table[bucket].count(value); // does set have a serach function or some other function that you would use here, to search within the set.
    }

private:
    vector<set<T> > table; // here we associate the T that SCTable accepts with the T that vector's set's type is.
    int tsize;

};

int main(int argc, char const *argv[]) {
    SCTable<int> s;

    // get number of items cmd line
    if (argc != 2){
        cerr << "usage: " << argv[0] << " nitems" << endl;
        return 1;
    }
    int nitems = atoi(argv[1]);
    //insert that many items into the set
    for (size_t i = 0; i < nitems; i++) {
        s.insert(i);
    }

    // find items in the set
    for (size_t i = 0; i < nitems; i++) {
        s.search(i);
    }

    return 0;
}
