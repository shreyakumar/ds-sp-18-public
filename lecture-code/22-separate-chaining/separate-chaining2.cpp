#include <iostream>
#include <vector> // for the table
#include <set> // for the buckets

using namespace std;

int TABLESIZE = 1<<5;

template <typename T>
class SCTable{
public:
    SCTable(int size=TABLESIZE){
        tsize = size;
        table = vector<set<T>>(tsize);// allocate table
    }

    T hash(const T &value){
        return value;
    }

    void insert(const T &value){ // insert fn for SCTable
        int bucket = hash(value) % tsize;
        table[bucket].insert(value); // set's insert function
    }

    bool search(const T &value){ // search for the hash table itself
        int bucket = hash(value) % tsize;
        return table[bucket].count(value); // count fn is used to serach in set
    }

private:
    vector<set<T>> table; // declared
    int tsize;
};

int main(int argc, char const *argv[]) {
    SCTable<int> s;

    // get number of items cmd line
    if (argc != 2){
        cerr << "usage: " << argv[0] << " nitems" << endl;
        return 1;
    }
    int nitems = atoi(argv[1]);
    //insert that many items into the set
    for (size_t i = 0; i < nitems; i++) {
        s.insert(i);
    }

    // find items in the set
    for (size_t i = 0; i < nitems; i++) {
        s.search(i);
    }

    return 0;
}
