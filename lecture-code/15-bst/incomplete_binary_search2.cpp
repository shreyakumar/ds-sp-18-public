// binary_search.cpp

#include <iostream>
#include <vector>

using namespace std;

// Recursive binary search with iterators
template <typename IT, typename T>
bool binary_search_r(IT start, IT end, const T &target) {
    auto length   = end - start;
    auto middle   = length / 2;
    auto midpoint = *(start + middle);

    //TODO recursive search for target
    // base case : nothing left to search
    if (start == end) return false; // not found
    // base case : target Found
    if (target == midpoint) return true;

    if (target < midpoint){
        return binary_search_r(start, start + middle, target);
    } else {
        return binary_search_r(start + middle + 1, end, target);
    }
}

// Iterative binary search with iterators
template <typename IT, typename T>
bool binary_search(IT start, IT end, const T &target) {
    while (start < end) {  // while there is still entries left to search.
    	auto length   = end - start;
    	auto middle   = length / 2;
    	auto midpoint = *(start + middle);

        if (target < midpoint){
            end = start + middle;
        } else if (target > midpoint){
            // do something else
            start = start + middle + 1;
        } else { // target == midpoint
            return true; // found!
        }
    } // end of while

    return false; // target not found
}

int main(int argc, char *argv[]) {
    vector<int> v0 = {0, 1, 2, 3, 4, 5};

    for (auto i : v0) { // searching for every single entry
    	cout << "Searching " << i << ": "
    	     << binary_search(v0.begin(), v0.end(), i)
    	     << endl;
    }

    for (auto i : vector<int>{-1, 7}) { // searching for more than just existing entries
    	cout << "Searching " << i << ": "
    	     << binary_search(v0.begin(), v0.end(), i)
    	     << endl;
    }

    return 0;
}
