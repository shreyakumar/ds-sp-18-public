// merge.cpp

#include "lsort.h"

#include <iostream>

// Prototypes

Node *msort(Node *head, CompareFunction compare);
void  split(Node *head, Node *&left, Node *&right);
Node *merge(Node *left, Node *right, CompareFunction compare);

// Implementations

void merge_sort(List &l, bool numeric) {
}

Node *msort(Node *head, CompareFunction compare) {
}

void split(Node *head, Node *&left, Node *&right) {
}

Node *merge(Node *left, Node *right, CompareFunction compare) {
}

// vim: set sts=4 sw=4 ts=8 expandtab ft=cpp:
